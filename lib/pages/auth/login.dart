import 'package:assessment/utils/dp.dart';
import 'package:assessment/utils/prefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _autoValidate = false;
  bool _obscureText = true, match = true;
  final _formKey = GlobalKey<FormState>();

  late String? email, password;

  String validateEmail(String? value) {
    Pattern pattern =
        r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]"
        r"{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]"
        r"{0,253}[a-zA-Z0-9])?)*$";
    RegExp regex = new RegExp(pattern.toString());
    if (value.toString().isEmpty) return 'Email can’t be empty';
    if (!regex.hasMatch(value.toString())) return 'Enter a valid email address';
    if (value.toString().length < 6)
      return "Email at least 6 characters";
    else
      return "";
  }

  @override
  Widget build(BuildContext context) {
    var dp = Dp(context, MediaQuery.of(context).size.width.toInt());
    return Scaffold(
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Container(
            padding: EdgeInsets.symmetric(
                horizontal: dp.dp(16), vertical: dp.dp(25)),
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 100),
                  child: Center(
                      child: Image.asset(
                    "assets/images/icon.png",
                    width: dp.dp(75),
                  )),
                ),
                SizedBox(
                  height: dp.dp(15),
                ),
                Text(
                  "Welcome!",
                  style: GoogleFonts.mulish(
                      fontSize: dp.dp(24), fontWeight: FontWeight.w800),
                ),
                SizedBox(
                  height: dp.dp(15),
                ),
                Text(
                  "Are you ready for The Game Changer",
                  style: GoogleFonts.mulish(
                      fontSize: dp.dp(16), fontWeight: FontWeight.normal),
                ),
                SizedBox(
                  height: dp.dp(50),
                ),
                TextFormField(
                  onChanged: (v) {
                    setState(() {
                      email = v;
                    });
                  },
                  onSaved: (v) {
                    setState(() {
                      email = v;
                    });
                  },
                  keyboardType: TextInputType.emailAddress,
                  cursorColor: Colors.black,
                  validator: validateEmail,
                  decoration: InputDecoration(
                      hintText: "Email",
                      labelText: "Email",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(dp.dp(10)),
                          borderSide:
                              BorderSide(width: 1, color: Colors.black26)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(dp.dp(10)),
                          borderSide:
                              BorderSide(width: 1, color: Colors.black26))),
                ),
                SizedBox(
                  height: dp.dp(10),
                ),
                TextFormField(
                  onChanged: (v) {
                    setState(() {
                      password = v;
                    });
                  },
                  onSaved: (v) {
                    setState(() {
                      password = v;
                    });
                  },
                  cursorColor: Colors.black,
                  obscureText: _obscureText,
                  validator: (value) {
                    if (value!.isEmpty)
                      return 'Password can’t be empty';
                    else if (value.length <= 6 && value.length <= 32)
                      return 'Password at least 6 characters & less than 32 characters';
                    return null;
                  },
                  decoration: InputDecoration(
                      hintText: "Password",
                      labelText: "Password",
                      suffixIcon: InkWell(
                        child: Icon(
                            _obscureText
                                ? Icons.visibility_off
                                : Icons.visibility,
                            color: Colors.black38),
                        onTap: () {
                          setState(() {
                            _obscureText = !_obscureText;
                          });
                        },
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(dp.dp(10)),
                          borderSide:
                              BorderSide(width: 1, color: Colors.black26)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(dp.dp(10)),
                          borderSide:
                              BorderSide(width: 1, color: Colors.black26))),
                ),
                SizedBox(
                  height: dp.dp(40),
                ),
                GestureDetector(
                  onTap: () {
                    FocusScopeNode currentFocus = FocusScope.of(context);

                    if (!currentFocus.hasPrimaryFocus) {
                      currentFocus.unfocus();
                    }

                    _formKey.currentState?.save();

                    if (email == "test@gmail.com" && password == "123456789") {
                      setState(() {
                        match = true;
                      });
                      setLogin(true);
                      Navigator.pushNamed(context, "/Home");
                    } else {
                      setState(() {
                        match = false;
                      });
                    }

                    if (_formKey.currentState!.validate()) {
                      Scaffold.of(context).showSnackBar(
                          SnackBar(content: Text('Processing Data')));
                    } else {
                      setState(() {
                        _autoValidate = true;
                      });
                    }
                  },
                  child: Container(
                    width: double.infinity,
                    height: dp.dp(50),
                    decoration: BoxDecoration(
                        color: Colors.indigo,
                        borderRadius: BorderRadius.circular(dp.dp(10))),
                    child: Center(
                      child: Text(
                        "Sign in",
                        style: GoogleFonts.mulish(
                            fontWeight: FontWeight.w400,
                            fontSize: dp.dp(14),
                            color: Colors.white),
                      ),
                    ),
                  ),
                ),
                match
                    ? Container()
                    : Padding(
                        padding: EdgeInsets.only(top: dp.dp(25)),
                        child: Text(
                          "Email and password do not match!",
                          style: GoogleFonts.mulish(
                              fontSize: dp.dp(14), color: Colors.red),
                        ),
                      )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
