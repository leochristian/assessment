import 'package:assessment/utils/prefs.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 3), () async {
      bool? data = await getLogin();
      if (data == true) {
        Navigator.pushNamedAndRemoveUntil(context, "/Home", (route) => false);
      } else {
        Navigator.pushReplacementNamed(context, "/Login");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Image.asset(
          "assets/images/icon.png",
          width: MediaQuery.of(context).size.width * 0.35,
        ),
      ),
    );
  }
}
