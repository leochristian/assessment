import 'package:assessment/pages/home/models/post_model.dart';
import 'package:assessment/pages/home/repositories/post_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'post_state.dart';

class PostCubit extends Cubit<PostState> {
  final PostRepository _postRepository = PostRepository();
  PostCubit() : super(InitialPostState());

  void post() async {
    emit(LoadingPostState());

    try {
      final data = await _postRepository.getPosts();

      emit(SuccessPostState(data));
    } catch (e) {
      emit(FailedPostState(e.toString()));
    }
  }
}
