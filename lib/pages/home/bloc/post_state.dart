part of 'post_cubit.dart';

abstract class PostState extends Equatable {
  const PostState();
}

class InitialPostState extends PostState {
  @override
  List<Object> get props => [];
}

class LoadingPostState extends PostState {
  @override
  List<Object> get props => [];
}

class SuccessPostState extends PostState {
  final List<PostModel> data;

  SuccessPostState(this.data);
  @override
  List<Object> get props => [data];
}

class FailedPostState extends PostState {
  final String error;

  FailedPostState(this.error);
  @override
  List<Object> get props => [error];
}
