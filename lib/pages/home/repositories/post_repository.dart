import 'package:assessment/pages/home/contracts/post_repository_contract.dart';
import 'package:assessment/pages/home/models/post_model.dart';
import '../data/post_api_service.dart';

class PostRepository implements PostRepositoryContrac {
  late PostApiService _apiService;

  PostRepository() {
    _apiService = PostApiService.create();
  }
  @override
  Future<List<PostModel>> getPosts() async {
    final response = await _apiService.getPosts();
    print(response.body);

    final body = response.body;

    final result = body.map<PostModel>((i) => PostModel.fromJson(i)).toList();

    return result;
  }
}
