import 'package:chopper/chopper.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

part 'post_api_service.chopper.dart';

@ChopperApi(baseUrl: "/")
abstract class PostApiService extends ChopperService {
  @Get(path: "posts")
  Future<Response> getPosts();

  static PostApiService create() {
    final baseUrl = dotenv.env["BASE_URL"];
    final client = ChopperClient(
      baseUrl: baseUrl.toString(),
      services: [
        _$PostApiService(),
      ],
      interceptors: [
        HttpLoggingInterceptor(),
        HeadersInterceptor({
          "Content-Type": "application/json",
          "Accept": "application/json",
        }),
      ],
      converter: JsonConverter(),
    );

    return _$PostApiService(client);
  }
}
