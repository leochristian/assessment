import 'package:assessment/pages/home/models/post_model.dart';

abstract class PostRepositoryContrac {
  Future<List<PostModel>> getPosts();
}
