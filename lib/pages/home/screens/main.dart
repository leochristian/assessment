import 'package:assessment/pages/home/bloc/post_cubit.dart';
import 'package:assessment/pages/home/models/post_model.dart';
import 'package:assessment/pages/home/screens/detail.dart';
import 'package:assessment/utils/dp.dart';
import 'package:assessment/utils/prefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final postCubit = PostCubit();

  List<PostModel> data = [];

  @override
  void initState() {
    super.initState();
    postCubit.post();
  }

  @override
  Widget build(BuildContext context) {
    var dp = Dp(context, MediaQuery.of(context).size.width.toInt());
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        shadowColor: Colors.black26,
        title: Text(
          "Post",
          textScaleFactor: 1,
          style: GoogleFonts.mulish(
              fontSize: dp.dp(16),
              color: Colors.black,
              fontWeight: FontWeight.w900),
        ),
        actions: [
          GestureDetector(
            onTap: () {
              setLogin(false);
              Navigator.pushNamedAndRemoveUntil(
                  context, "/Login", (route) => false);
            },
            child: Icon(
              Icons.logout_outlined,
              color: Colors.black,
            ),
          ),
          SizedBox(
            width: dp.dp(20),
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: BlocProvider<PostCubit>(
        create: (context) => postCubit,
        child: BlocListener<PostCubit, PostState>(
          listener: (context, state) {
            if (state is LoadingPostState || state is InitialPostState) {}

            if (state is SuccessPostState) {
              setState(() {
                data.addAll(state.data);
              });
            }
          },
          child: BlocBuilder<PostCubit, PostState>(builder: (context, state) {
            if (state is LoadingPostState || state is InitialPostState) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }

            if (state is FailedPostState) {
              return Center(
                child: Text(
                  "Error",
                  textScaleFactor: 1,
                  style: GoogleFonts.mulish(
                      fontSize: dp.dp(12),
                      color: Colors.white,
                      fontWeight: FontWeight.w900),
                ),
              );
            }

            if (state is SuccessPostState) {
              if (data.length == 0) {
                return Center(
                  child: Text(
                    "Post is empty",
                    textScaleFactor: 1,
                    style: GoogleFonts.mulish(
                        fontSize: dp.dp(12),
                        color: Colors.white,
                        fontWeight: FontWeight.w900),
                  ),
                );
              } else {
                return SingleChildScrollView(
                  child: Column(
                    children: data
                        .map((e) => GestureDetector(
                              onTap: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => DetailPost(
                                            data: e,
                                          ))),
                              child: Container(
                                margin: EdgeInsets.symmetric(
                                    vertical: dp.dp(7.5),
                                    horizontal: dp.dp(16)),
                                width: double.infinity,
                                padding: EdgeInsets.all(dp.dp(16)),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.5),
                                        spreadRadius: 1,
                                        blurRadius: 1,
                                        offset: Offset(
                                            0, 2), // changes position of shadow
                                      ),
                                    ],
                                    borderRadius:
                                        BorderRadius.circular(dp.dp(10))),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      e.title.toString(),
                                      textScaleFactor: 1,
                                      style: GoogleFonts.mulish(
                                          fontSize: dp.dp(14),
                                          color: Colors.black,
                                          fontWeight: FontWeight.w700),
                                    ),
                                    Text(
                                      e.body.toString(),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      textScaleFactor: 1,
                                      style: GoogleFonts.mulish(
                                          fontSize: dp.dp(14),
                                          color: Colors.black,
                                          fontWeight: FontWeight.normal),
                                    ),
                                  ],
                                ),
                              ),
                            ))
                        .toList(),
                  ),
                );
              }
            }

            return Container();
          }),
        ),
      ),
    );
  }
}
