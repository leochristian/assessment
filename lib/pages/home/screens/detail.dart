import 'package:assessment/pages/home/models/post_model.dart';
import 'package:assessment/utils/dp.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class DetailPost extends StatefulWidget {
  final PostModel? data;

  const DetailPost({Key? key, this.data}) : super(key: key);

  @override
  _DetailPostState createState() => _DetailPostState();
}

class _DetailPostState extends State<DetailPost> {
  @override
  Widget build(BuildContext context) {
    var dp = Dp(context, MediaQuery.of(context).size.width.toInt());
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        shadowColor: Colors.black26,
        leading: InkWell(
          onTap: () => Navigator.pop(context),
          child: Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
            size: dp.dp(14),
          ),
        ),
        title: Text(
          "Detail",
          textScaleFactor: 1,
          style: GoogleFonts.mulish(
              fontSize: dp.dp(16),
              color: Colors.black,
              fontWeight: FontWeight.w900),
        ),
      ),
      backgroundColor: Colors.white,
      body: Container(
        padding:
            EdgeInsets.symmetric(horizontal: dp.dp(16), vertical: dp.dp(25)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              widget.data!.title.toString(),
              textScaleFactor: 1,
              style: GoogleFonts.mulish(
                  fontSize: dp.dp(16),
                  color: Colors.black,
                  fontWeight: FontWeight.w700),
            ),
            SizedBox(height: dp.dp(10)),
            Text(
              widget.data!.body.toString(),
              textScaleFactor: 1,
              style: GoogleFonts.mulish(
                  fontSize: dp.dp(14),
                  color: Colors.black,
                  fontWeight: FontWeight.normal),
            ),
          ],
        ),
      ),
    );
  }
}
