import 'package:shared_preferences/shared_preferences.dart';

Future<void> setLogin(bool token) async {
  final _p = await SharedPreferences.getInstance();
  _p.setBool("isLogin", token);
}

Future<bool?> getLogin() async {
  final _p = await SharedPreferences.getInstance();

  return _p.getBool("isLogin");
}
