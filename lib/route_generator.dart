import 'package:assessment/pages/auth/login.dart';
import 'package:assessment/pages/home/screens/main.dart';
import 'package:assessment/pages/splash/main.dart';
import 'package:flutter/material.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings? settings) {
    final args = settings?.arguments;
    switch (settings?.name) {
      case '/Splash':
        return MaterialPageRoute(
          builder: (_) => SplashScreen(),
        );
      case '/Home':
        return MaterialPageRoute(
          builder: (_) => HomePage(),
        );
      case '/Login':
        return MaterialPageRoute(
          builder: (_) => LoginScreen(),
        );
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: SafeArea(
              child: Text('Route Error'),
            ),
          ),
        );
    }
  }
}
